import jwt from 'jsonwebtoken';
import bcrypt from "bcryptjs";

export const encryptPassword = (password: string) => new Promise<string>((resolve, reject) => {
	bcrypt.genSalt(10, (err: any, salt: any) => {
		if (err) {
			reject(err)
			return false
		}
		bcrypt.hash(password, salt, (err: any, hash: any) => {
			if (err) {
				reject(err)
				return false
			}
			resolve(hash)
			return true
		})
	})
})

export const comparePassword = (password: string, hash: any) => new Promise(async (resolve, reject) => {
	try {
		const isMatch = await bcrypt.compare(password, hash)
		resolve(isMatch)
		return true
	} catch (err) {
		reject(err)
		return false
	}
})

export const getToken = (payload: any) => {
    const token = jwt.sign({payload}, process.env.SECRET!, {
        expiresIn: 604800, // 1 Week 
    })
    return token
}

export const getPayload = (token: string) => {
    try {
        const payload = jwt.verify(token, process.env.SECRET!);
        return { loggedIn: true, payload };
    } catch (err) {
        return { loggedIn: false, payload: null }
    }
}
