import { User } from "../entity/User"
import { Field, ObjectType } from "type-graphql"

@ObjectType()
export class TokenizedUser {
    @Field(() => User)
    user: User
    @Field()
    token: string
}