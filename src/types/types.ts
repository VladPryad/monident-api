import { Field, InputType, ObjectType } from "type-graphql"
import { Connection } from "typeorm"


@ObjectType()
export class Frequency {
    @Field()
    UV?: String
    @Field()
    IR?: String
    @Field()
    V?: String
}

@InputType()
export class FrequencyInput {
    @Field()
    UV?: String
    @Field()
    IR?: String
    @Field()
    V?: String
}
export type Context = {
    conn: Connection,
    id: string,
    loggedIn: boolean
}

export interface IPayload {
    payload: { id: number },
    iat: number,
    exp: number
}