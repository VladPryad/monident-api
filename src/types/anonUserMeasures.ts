import { User } from "../entity/User"
import { Field, ObjectType } from "type-graphql"
import { UserExtendedData } from "../entity/UserExtendedData"
import { Measure } from "../entity/Measure"

@ObjectType()
export class AnonymizedUser {
    @Field()
    id: number

    @Field(() => UserExtendedData, { nullable: true })
    details?: UserExtendedData

    @Field(() => [Measure!], { nullable: true })
    measures?: Measure[]
}