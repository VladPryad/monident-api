import { Field, ObjectType } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { Frequency } from '../types/types';

@ObjectType()
@Entity()
export class DentalParams extends BaseEntity {

    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column({ nullable: true, type: "simple-json" })
    albedo?: Frequency;

    @Field()
    @Column({ nullable: true, type: "simple-json" })
    transparency?: Frequency

    @Field()
    @Column({ nullable: true })
    density?: number

}