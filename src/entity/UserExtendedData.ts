import { Field, ObjectType } from "type-graphql";
import {Entity, PrimaryGeneratedColumn, Column, BaseEntity} from "typeorm";

@ObjectType()
@Entity()
export class UserExtendedData extends BaseEntity {

    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column({ nullable: true })
    age?: number;

    @Field()
    @Column({ nullable: true })
    gender?: string;

    @Field()
    @Column({ nullable: true })
    smoker?: boolean;

    @Field()
    @Column({ nullable: true })
    toothpaste?: string;

}
