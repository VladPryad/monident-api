import { Field, ObjectType } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne, JoinColumn, OneToMany } from "typeorm";
import { Measure } from "./Measure";
import { UserExtendedData } from "./UserExtendedData";

@ObjectType()
@Entity()
export class User extends BaseEntity {

    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field(() => String, { nullable: true })
    @Column({ nullable: true })
    name?: string;

    @Field(() => String, { nullable: true })
    @Column({ nullable: true })
    surname?: string;

    @Field()
    @Column({ nullable: true, unique: true })
    email: string;

    @Field()
    @Column()
    password: string;

    @Field(() => [Measure!])
    @OneToMany(() => Measure, measure => measure.user, {onDelete: 'CASCADE', onUpdate: 'CASCADE'})
    measures: Measure[];

    @Field()
    @Column({ nullable: true })
    userExtendedDataId: number;

    @Field(() => UserExtendedData, { nullable: true })
    @OneToOne(() => UserExtendedData, {onDelete: 'CASCADE', onUpdate: 'CASCADE'})
    @JoinColumn({name: "userExtendedDataId"})
    userExtendedData: UserExtendedData;

}
