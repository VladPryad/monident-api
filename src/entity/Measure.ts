import { Field, ObjectType } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, BaseEntity, ManyToOne } from "typeorm";
import { DentalParams } from "./DentalParams"
import { User } from "./User"

@ObjectType()
@Entity()
export class Measure extends BaseEntity {

    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column({ type: "timestamp" })
    date: string;

    @Column()
    userId: number;

    @ManyToOne(() => User, user => user.measures, { eager: true })
    @JoinColumn({ name: "userId" })
    user: User;

    @Field()
    @Column({ nullable: true })
    DentalParamsId: number;

    @Field()
    @OneToOne(() => DentalParams, { onDelete: 'CASCADE', onUpdate: 'CASCADE', eager: true })
    @JoinColumn({ name: "DentalParamsId" })
    DentalParams: DentalParams;

}