export function passCheck(info: any) {
    function check(parentField: any) {
      if (parentField == undefined) return; // recursion base
      parentField = parentField.selections;
      for (let field of parentField) {
        if (field.name.value == "password" || field.name.value == "token") {
          throw new Error("Password or token must not be requested");
        }
        check(field.selectionSet);
      }
    }
  
    check(info.operation.selectionSet.selections[0].selectionSet);
  }