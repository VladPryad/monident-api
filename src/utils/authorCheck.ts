import { Context } from "../types/types";

export function authorCheck(ctx: Context, idInternal: string): void {
    let {loggedIn, id} = ctx;
    if (!loggedIn) throw new Error("Unauthorized 401");
    if (id != idInternal) throw new Error("Unauthorized 401");
}

export function cutCheck(ctx: Context): void {
    if (!ctx.loggedIn) throw new Error("Unauthorized 401");
}