import { Measure } from "../../../entity/Measure";

export interface IMediator {
    median(arr: Measure[]): Measure;
}