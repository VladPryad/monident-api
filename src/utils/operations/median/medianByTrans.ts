import { Measure } from "../../../entity/Measure";
import { SortByTransIRAsc, SortByTransUVAsc, SortByTransVAsc } from "../sort/byAsc/sorterByTransAsc";
import { IMediator } from "./imediator";


export class MedianByTransUV implements IMediator {
    median(arr: Measure[]): Measure {
        arr = (new SortByTransUVAsc).sort(arr);
        let measure: Measure = (arr.length % 2 != 0) ? arr[(arr.length - 1) / 2] : arr[(arr.length / 2)];
        return measure;
    }
}

export class MedianByTransIR implements IMediator {
    median(arr: Measure[]): Measure {
        arr = (new SortByTransIRAsc).sort(arr);
        let measure: Measure = (arr.length % 2 != 0) ? arr[(arr.length - 1) / 2] : arr[(arr.length / 2)];
        return measure;
    }
}

export class MedianByTransV implements IMediator {
    median(arr: Measure[]): Measure {
        arr = (new SortByTransVAsc).sort(arr);
        let measure: Measure = (arr.length % 2 != 0) ? arr[(arr.length - 1) / 2] : arr[(arr.length / 2)];
        return measure;
    }
}