import { Measure } from "../../../entity/Measure";
import { SortByAlbedoIRAsc, SortByAlbedoUVAsc, SortByAlbedoVAsc } from "../sort/byAsc/sorterByAlbedoAsc";
import { IMediator } from "./imediator";


export class MedianByAlbedoUV implements IMediator {
    median(arr: Measure[]): Measure {
        arr = (new SortByAlbedoUVAsc).sort(arr);
        console.log(arr);
        let measure: Measure = (arr.length % 2 != 0) ? arr[(arr.length - 1) / 2] : arr[(arr.length / 2)];
        console.log(measure);
        return measure;
    }
}

export class MedianByAlbedoIR implements IMediator {
    median(arr: Measure[]): Measure {
        arr = (new SortByAlbedoIRAsc).sort(arr);
        let measure: Measure = (arr.length % 2 != 0) ? arr[(arr.length - 1) / 2] : arr[(arr.length / 2)];
        return measure;
    }
}

export class MedianByAlbedoV implements IMediator {
    median(arr: Measure[]): Measure {
        arr = (new SortByAlbedoVAsc).sort(arr);
        let measure: Measure = (arr.length % 2 != 0) ? arr[(arr.length - 1) / 2] : arr[(arr.length / 2)];
        return measure;
    }
}