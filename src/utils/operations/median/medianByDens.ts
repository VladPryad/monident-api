import { Measure } from "../../../entity/Measure";
import { SortByDensAsc } from "../sort/byAsc/sorterByDensAsc";
import { IMediator } from "./imediator";


export class MedianByDens implements IMediator {
    median(arr: Measure[]): Measure {
        arr = (new SortByDensAsc).sort(arr);
        let measure: Measure = (arr.length % 2 != 0) ? arr[(arr.length - 1) / 2] : arr[(arr.length / 2)];
        return measure;
    }
}