import { Measure } from "../../../entity/Measure";
import { IMediator } from "./imediator";

export class Mediator{
    mediator(mediator: IMediator, arr: Measure[]) : Measure {
        return mediator.median(arr);
    }
}