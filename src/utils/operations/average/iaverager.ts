import { Measure } from "../../../entity/Measure";

export interface IAverager {
    average(arr: Measure[]): number;

}