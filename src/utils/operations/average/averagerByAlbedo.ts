import { Measure } from "src/entity/Measure";
import { IAverager } from "./iaverager";

export class AvarageByAlbedoUV implements IAverager {
    average(arr: Measure[]): number {
        let sum = 0;
        let count = 0;
        for(let measure of arr) {
            let curr = measure.DentalParams.albedo!.UV;
            let value: number = curr ? +curr : 0; 
            if(typeof curr !== "undefined") count++;
            sum += value;
        }
        
        return sum/count;
    }
}

export class AvarageByAlbedoIR implements IAverager {
    average(arr: Measure[]): number {
        let sum = 0;
        let count = 0;
        for(let measure of arr) {
            let curr = measure.DentalParams.albedo!.IR;
            let value: number = curr ? +curr : 0; 
            if(typeof curr !== "undefined") count++;
            sum += value;
        }
        
        return sum/count;
    }
}

export class AvarageByAlbedoV implements IAverager {
    average(arr: Measure[]): number {
        let sum = 0;
        let count = 0;
        for(let measure of arr) {
            let curr = measure.DentalParams.albedo!.V;
            let value: number = curr ? +curr : 0; 
            if(typeof curr !== "undefined") count++;
            sum += value;
        }
        
        return sum/count;
    }
}