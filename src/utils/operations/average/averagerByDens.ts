import { Measure } from "src/entity/Measure";
import { IAverager } from "./iaverager";

export class AvarageByDens implements IAverager {
    average(arr: Measure[]): number {
        let sum = 0;
        let count = 0;
        for(let measure of arr) {
            let curr = measure.DentalParams.density;
            let value: number = curr ? +curr : 0; 
            if(typeof curr !== "undefined") count++;
            sum += value;
        }
        
        return sum/count;
    }
}