import { Measure } from "src/entity/Measure";
import { IAverager } from "./iaverager";

export class AvarageByTransUV implements IAverager {
    average(arr: Measure[]): number {
        let sum = 0;
        let count = 0;
        for(let measure of arr) {
            let curr = measure.DentalParams.transparency!.UV;
            let value: number = curr ? +curr : 0; 
            if(typeof curr !== "undefined") count++;
            sum += value;
        }
        
        return sum/count;
    }
}

export class AvarageByTransIR implements IAverager {
    average(arr: Measure[]): number {
        let sum = 0;
        let count = 0;
        for(let measure of arr) {
            let curr = measure.DentalParams.transparency!.IR;
            let value: number = curr ? +curr : 0; 
            if(typeof curr !== "undefined") count++;
            sum += value;
        }
        
        return sum/count;
    }
}

export class AvarageByTransV implements IAverager {
    average(arr: Measure[]): number {
        let sum = 0;
        let count = 0;
        for(let measure of arr) {
            let curr = measure.DentalParams.transparency!.V;
            let value: number = curr ? +curr : 0; 
            if(typeof curr !== "undefined") count++;
            sum += value;
        }
        
        return sum/count;
    }
}