import { Measure } from "../../../entity/Measure";
import { IAverager } from "./iaverager";

export class Averager {
    average(averager: IAverager, arr: Measure[]) : number {
        return averager.average(arr);
    }
}