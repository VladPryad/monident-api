import { Measure } from "../../../entity/Measure";
import { IMinMaxer } from "./iminmaxer";

export class MinMaxByTransUV implements IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure] {
        let max = arr[0];
        let min = arr[0];
        for (let curr of arr) {
            max = max.DentalParams.transparency!.UV! >= curr.DentalParams.transparency!.UV! ? max : curr;
            min = min.DentalParams.transparency!.UV! <= curr.DentalParams.transparency!.UV! ? min : curr;
        }
        return [min, max];
    }
}

export class MinMaxByTransIR implements IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure] {
        let max = arr[0];
        let min = arr[0];
        for (let curr of arr) {
            max = max.DentalParams.transparency!.IR! >= curr.DentalParams.transparency!.IR! ? max : curr;
            min = min.DentalParams.transparency!.IR! <= curr.DentalParams.transparency!.IR! ? min : curr;
        }
        return [min, max];
    }
}

export class MinMaxByTransV implements IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure] {
        let max = arr[0];
        let min = arr[0];
        for (let curr of arr) {
            max = max.DentalParams.transparency!.V! >= curr.DentalParams.transparency!.V! ? max : curr;
            min = min.DentalParams.transparency!.V! <= curr.DentalParams.transparency!.V! ? min : curr;
        }
        return [min, max];
    }
}