import { Measure } from "../../../entity/Measure";
import { IMinMaxer } from "./iminmaxer";

export class MinMaxByDens implements IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure] {
        let max = arr[0];
        let min = arr[0];
        for (let curr of arr) {
            max = max.DentalParams.density! >= curr.DentalParams.density! ? max : curr;
            min = min.DentalParams.density! <= curr.DentalParams.density! ? min : curr;
        }
        return [min, max];
    }
}