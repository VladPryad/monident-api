import { Measure } from "../../../entity/Measure";

export interface IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure];
}