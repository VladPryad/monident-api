import { Measure } from "../../../entity/Measure";
import { IMinMaxer } from "./iminmaxer";

export class MinMaxer{
    minmaxer(minmaxer: IMinMaxer, arr: Measure[]) : [min: Measure, max: Measure] {
        return minmaxer.minmax(arr);
    }
}