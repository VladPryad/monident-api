import { Measure } from "../../../entity/Measure";
import { IMinMaxer } from "./iminmaxer";

export class MinMaxByAlbedoUV implements IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure] {
        let max = arr[0];
        let min = arr[0];
        for (let curr of arr) {
            max = max.DentalParams.albedo!.UV! >= curr.DentalParams.albedo!.UV! ? max : curr;
            min = min.DentalParams.albedo!.UV! <= curr.DentalParams.albedo!.UV! ? min : curr;
        }
        return [min, max];
    }
}

export class MinMaxByAlbedoIR implements IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure] {
        let max = arr[0];
        let min = arr[0];
        for (let curr of arr) {
            max = max.DentalParams.albedo!.IR! >= curr.DentalParams.albedo!.IR! ? max : curr;
            min = min.DentalParams.albedo!.IR! <= curr.DentalParams.albedo!.IR! ? min : curr;
        }
        return [min, max];
    }
}

export class MinMaxByAlbedoV implements IMinMaxer {
    minmax(arr: Measure[]): [min: Measure, max: Measure] {
        let max = arr[0];
        let min = arr[0];
        for (let curr of arr) {
            max = max.DentalParams.albedo!.V! >= curr.DentalParams.albedo!.V! ? max : curr;
            min = min.DentalParams.albedo!.V! <= curr.DentalParams.albedo!.V! ? min : curr;
        }
        return [min, max];
    }
}