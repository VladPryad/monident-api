import { Measure } from "src/entity/Measure";
import { ISorter } from "../isorter";

export class SortByAlbedoUVAsc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.albedo!.UV;
            let second = b.DentalParams.albedo!.UV;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  x - y; 
        });
        return arr;
    }
}

export class SortByAlbedoIRAsc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.albedo!.IR;
            let second = b.DentalParams.albedo!.IR;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  x - y; 
        });
        return arr;
    }
}

export class SortByAlbedoVAsc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.albedo!.V;
            let second = b.DentalParams.albedo!.V;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  x - y; 
        });
        return arr;
    }
}