import { Measure } from "src/entity/Measure";
import { ISorter } from "../isorter";

export class SortByTransUVAsc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.transparency!.UV;
            let second = b.DentalParams.transparency!.UV;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  x - y; 
        });
        return arr;
    }
}

export class SortByTransIRAsc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.transparency!.IR;
            let second = b.DentalParams.transparency!.IR;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  x - y; 
        });
        return arr;
    }
}

export class SortByTransVAsc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.transparency!.V;
            let second = b.DentalParams.transparency!.V;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  x - y; 
        });
        return arr;
    }
}