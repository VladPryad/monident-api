import { Measure } from "src/entity/Measure";
import { ISorter } from "../isorter";

export class SortByDensAsc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.density;
            let second = b.DentalParams.density;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  x - y; 
        });
        return arr;
    }
}
