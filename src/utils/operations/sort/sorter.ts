import { Measure } from "../../../entity/Measure";
import { ISorter } from "./isorter";

export class Sorter {
    sort(sorter: ISorter, arr: Measure[]) : Measure[] {
        return sorter.sort(arr);
    }
}