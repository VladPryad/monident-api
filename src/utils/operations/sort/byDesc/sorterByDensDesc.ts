import { Measure } from "src/entity/Measure";
import { ISorter } from "../isorter";

export class SortByDensDesc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.density;
            let second = b.DentalParams.density;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  y - x; 
        });
        return arr;
    }
}
