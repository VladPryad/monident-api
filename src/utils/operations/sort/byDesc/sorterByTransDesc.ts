import { Measure } from "src/entity/Measure";
import { ISorter } from "../isorter";

export class SortByTransUVDesc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.transparency!.UV;
            let second = b.DentalParams.transparency!.UV;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  y - x; 
        });
        return arr;
    }
}

export class SortByTransIRDesc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.transparency!.IR;
            let second = b.DentalParams.transparency!.IR;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  y - x; 
        });
        return arr;
    }
}

export class SortByTransVDesc implements ISorter {
    sort(arr: Measure[]): Measure[] {

        arr.sort(function (a: Measure, b: Measure) {
            let first = a.DentalParams.transparency!.V;
            let second = b.DentalParams.transparency!.V;
            let x = !!first ? +first : 0;
            let y = !!second ? +second : 0;
            return  y - x; 
        });
        return arr;
    }
}