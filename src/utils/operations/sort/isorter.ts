import { Measure } from "../../../entity/Measure";

export interface ISorter {
    sort(arr: Measure[]): Measure[];

}