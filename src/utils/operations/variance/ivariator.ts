import { Measure } from "../../../entity/Measure";

export interface IVariator {
    variance(arr: Measure[]): number;

}