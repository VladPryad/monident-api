import { Measure } from "src/entity/Measure";
import { AvarageByDens } from "../average/averagerByDens";
import { IVariator } from "./ivariator";

export class VarianceByDens implements IVariator {
    variance(arr: Measure[]): number {
        if(arr.length == 0) return 0;
        const averager = new AvarageByDens();
        let avgPowered = Math.pow(averager.average(arr), 2);

        for (let measure of arr) {
            let value = +(measure.DentalParams.density!);
            measure.DentalParams.density = Math.pow(value, 2);
        }  
        let avg = averager.average(arr);

        return (avg - avgPowered) / arr.length;
    }
}