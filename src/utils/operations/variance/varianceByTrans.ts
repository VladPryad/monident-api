import { Measure } from "src/entity/Measure";
import { AvarageByTransIR, AvarageByTransUV, AvarageByTransV } from "../average/averagerByTrans";
import { IVariator } from "./ivariator";

export class VarianceByTransUV implements IVariator {
    variance(arr: Measure[]): number {
        if(arr.length == 0) return 0;
        const averager = new AvarageByTransUV();
        let avgPowered = Math.pow(averager.average(arr), 2);

        for (let measure of arr) {
            let value = +(measure.DentalParams.transparency!.UV!);
            measure.DentalParams.transparency!.UV = Math.pow(value, 2).toString();
        }  
        let avg = averager.average(arr);

        return (avg - avgPowered) / arr.length;
    }
}

export class VarianceByTransIR implements IVariator {
    variance(arr: Measure[]): number {
        if(arr.length == 0) return 0;
        const averager = new AvarageByTransIR();
        let avgPowered = Math.pow(averager.average(arr), 2);

        for (let measure of arr) {
            let value = +(measure.DentalParams.transparency!.IR!);
            measure.DentalParams.transparency!.UV = Math.pow(value, 2).toString();
        }  
        let avg = averager.average(arr);

        return (avg - avgPowered) / arr.length;
    }
}

export class VarianceByTransV implements IVariator {
    variance(arr: Measure[]): number {
        if(arr.length == 0) return 0;
        const averager = new AvarageByTransV();
        let avgPowered = Math.pow(averager.average(arr), 2);

        for (let measure of arr) {
            let value = +(measure.DentalParams.transparency!.V!);
            measure.DentalParams.transparency!.UV = Math.pow(value, 2).toString();
        }  
        let avg = averager.average(arr);

        return (avg - avgPowered) / arr.length;
    }
}