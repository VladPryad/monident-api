import { Measure } from "../../../entity/Measure";
import { IVariator } from "./ivariator";

export class Variator {
    varience(variator: IVariator, arr: Measure[]) : number {
        return variator.variance(arr);
    }
}