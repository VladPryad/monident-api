import { Measure } from "src/entity/Measure";
import { AvarageByAlbedoIR, AvarageByAlbedoUV, AvarageByAlbedoV } from "../average/averagerByAlbedo";
import { IVariator } from "./ivariator";

export class VarianceByAlbedoUV implements IVariator {
    variance(arr: Measure[]): number {
        if(arr.length == 0) return 0;
        const averager = new AvarageByAlbedoUV();
        let avgPowered = Math.pow(averager.average(arr), 2);

        for (let measure of arr) {
            let value = +(measure.DentalParams.albedo!.UV!);
            measure.DentalParams.albedo!.UV = Math.pow(value, 2).toString();
        }  
        let avg = averager.average(arr);

        return (avg - avgPowered) / arr.length;
    }
}

export class VarianceByAlbedoIR implements IVariator {
    variance(arr: Measure[]): number {
        if(arr.length == 0) return 0;
        const averager = new AvarageByAlbedoIR();
        let avgPowered = Math.pow(averager.average(arr), 2);

        for (let measure of arr) {
            let value = +(measure.DentalParams.albedo!.IR!);
            measure.DentalParams.albedo!.UV = Math.pow(value, 2).toString();
        }  
        let avg = averager.average(arr);

        return (avg - avgPowered) / arr.length;
    }
}

export class VarianceByAlbedoV implements IVariator {
    variance(arr: Measure[]): number {
        if(arr.length == 0) return 0;
        const averager = new AvarageByAlbedoV();
        let avgPowered = Math.pow(averager.average(arr), 2);

        for (let measure of arr) {
            let value = +(measure.DentalParams.albedo!.V!);
            measure.DentalParams.albedo!.UV = Math.pow(value, 2).toString();
        }  
        let avg = averager.average(arr);

        return (avg - avgPowered) / arr.length;
    }
}