import { User } from "../entity/User";
import { Arg, Ctx, Float, Info, Int, Mutation, Query, Resolver } from "type-graphql";
import { Context, Frequency, FrequencyInput } from '../types/types'
import { passCheck } from '../utils/passCheck'
import { authorCheck, cutCheck } from "../utils/authorCheck";
import { Measure } from "../entity/Measure";
import { DentalParams } from "../entity/DentalParams";
import { MinMaxer } from "../utils/operations/minmax/minmaxer";
import { MinMaxByAlbedoUV } from "../utils/operations/minmax/minmaxByAlbedo";
import { Variator } from "../utils/operations/variance/variator";
import { VarianceByAlbedoUV } from "../utils/operations/variance/varianceByAlbedo";


@Resolver()
export class DentalResolver {
    //#region Query
    @Query(() => Measure)
    async getMeasureById(
        @Arg("id") id: string,
        @Ctx() ctx: Context,
        @Ctx() { conn }: Context,
        @Info() info: any
    ): Promise<Measure> {
        passCheck(info);
        const measureRepo = conn.manager.getRepository(Measure);
        const measure = await measureRepo.findOne(id, { relations: ["DentalParams"] });
        if (!measure) throw new Error("Measure not found 404");
        authorCheck(ctx, measure.userId.toString());
        return measure;
    }

    @Query(() => [Measure, Measure])
    async test(
        @Ctx() ctx: Context,
        @Ctx() { conn }: Context,
        @Info() info: any
    ): Promise<[Measure, Measure]> {
        const measureRepo = conn.manager.getRepository(User);
        const measure = await measureRepo.findOne(3, { relations: ["measures"] });
        let med = new MinMaxer();
        return med.minmaxer(new MinMaxByAlbedoUV(), measure!.measures)
    }

    @Query(() => Float)
    async variance(
        @Ctx() ctx: Context,
        @Ctx() { conn }: Context,
        @Info() info: any
    ): Promise<number> {
        const measureRepo = conn.manager.getRepository(User);
        const measure = await measureRepo.findOne(3, { relations: ["measures"] });
        let variator = new Variator();
        return variator.varience(new VarianceByAlbedoUV(), measure!.measures);
    }

    //#endregion

    //#region Mutation
    @Mutation(() => Measure)
    async addMeasure(
        @Arg("albedo") albedo: FrequencyInput,
        @Arg("transparency") transparency: FrequencyInput,
        @Arg("density") density: number,
        @Ctx() { conn, id }: Context,
        @Ctx() ctx: Context,
    ): Promise<Measure> {
        cutCheck(ctx);
        const measureRepo = conn.manager.getRepository(Measure);
        const dentalRepo = conn.manager.getRepository(DentalParams);
        const userRepo = conn.manager.getRepository(User);

        let user = await userRepo.findOne(id);
        if (!user) throw new Error("User not found 404");

        try {
            let params = new DentalParams();
            params.albedo = albedo;
            params.transparency = transparency;
            params.density = density;

            await dentalRepo.save(params);

            let measure = new Measure();

            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;

            measure.date = dateTime;

            measure.DentalParams = params;
            measure.DentalParamsId = params.id;
            measure.user = user;
            measure.userId = user.id;

            await measureRepo.save(measure);

            return measure;
        } catch (e) {
            throw e;
        }
    };
    //#endregion
}