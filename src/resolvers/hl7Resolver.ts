import { User } from "../entity/User";
import { Arg, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { Context, Frequency, FrequencyInput } from '../types/types'
import { passCheck } from '../utils/passCheck'
import { authorCheck, cutCheck } from "../utils/authorCheck";
import { Measure } from "../entity/Measure";
import { DentalParams } from "../entity/DentalParams";
import { AnonymizedUser } from "../types/anonUserMeasures";

@Resolver()
export class HL7Resolver {
    
    @Query(() => [AnonymizedUser!])
    async getData(
        @Ctx() ctx: Context,
        @Ctx() { conn }: Context,
        @Info() info: any
    ): Promise<AnonymizedUser[]> {
        cutCheck(ctx);
        passCheck(info);

        const userRepo = conn.manager.getRepository(User);
        const users = await userRepo.find({relations: ["userExtendedData", "measures"]});
        let anonUsersData = [];

        if(!users) throw new Error("No users");
        
        let id = 0;
        for(let user of users ) {
            let anonymized = new AnonymizedUser();
            anonymized.id = id++;
            anonymized.details = user.userExtendedData;
            anonymized.measures = user.measures;
            anonUsersData.push(anonymized);
        }

        return anonUsersData;
    }
}