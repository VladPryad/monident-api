import { User } from "../entity/User";
import { Arg, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { Context } from '../types/types'
import { comparePassword, encryptPassword, getToken } from "../token";
import { TokenizedUser } from "../types/extendedReturnTypes";
import { passCheck } from '../utils/passCheck'
import { UserExtendedData } from "../entity/UserExtendedData";
import { authorCheck, cutCheck } from "../utils/authorCheck";
import { Measure } from "../entity/Measure";

@Resolver()
export class UserResolver {
    //#region Query

    @Query(() => User)
    async getUserById(
        @Arg("id") id: string,
        @Ctx() ctx: Context,
        @Ctx() { conn }: Context,
        @Info() info: any
    ) {
        passCheck(info);
        authorCheck(ctx, id);
        const userRepo = conn.manager.getRepository(User);
        const extdUser = await userRepo.findOne(id, { relations: ["userExtendedData", "measures"] });
        if (!extdUser) throw new Error("No such user");
        return extdUser;
    }

    @Query(() => [Measure])
    async getAllUserMeasures(
        @Arg("id") id: string,
        @Ctx() ctx: Context,
        @Ctx() { conn }: Context,
        @Info() info: any
    ): Promise<Measure[]> {
        passCheck(info);
        authorCheck(ctx, id);
        const userRepo = conn.manager.getRepository(User);
        const extdUser = await userRepo.findOne(id, { relations: ["measures"] });
        if (!extdUser) throw new Error("No such user");
        return extdUser.measures;
    }

    //#endregion

    //#region Mutation
    @Mutation(() => TokenizedUser)
    async register(
        @Arg("email") email: string,
        @Arg("name", () => String, { nullable: true }) name: string,
        @Arg("password") password: string,
        @Ctx() { conn }: Context,
    ): Promise<TokenizedUser> {
        const person = await conn.manager.find(User, { email });
        if (person.length != 0) throw new Error("User Already Exists!");
        try {
            const userRepo = conn.manager.getRepository(User);
            const user = new User();
            user.email = email;
            user.name = name;
            user.password = await encryptPassword(password);
            await userRepo.save(user);
            const token = getToken({ id: user.id });
            return { user, token };
        } catch (e) {
            throw e;
        }
    }

    @Mutation(() => String)
    async login(
        @Arg("email") email: string,
        @Arg("password") password: string,
        @Ctx() { conn }: Context
    ): Promise<String> {
        const person = await conn.manager.findOne(User, { email });
        if (!person) throw new Error("No such user");
        const isMatch = await comparePassword(password, person.password)
        if (!isMatch) throw new Error("Wrong Login or Password!")
        const token = getToken({ id: person.id })
        return token;
    }

    @Mutation(() => User)
    async addUserExtendedData(
        @Arg("age", { nullable: true }) age: number,
        @Arg("gender", { nullable: true }) gender: string,
        @Arg("smoker", { nullable: true }) smoker: boolean,
        @Arg("toothpaste", { nullable: true }) toothpaste: string,
        @Ctx() { conn, id, loggedIn }: Context,
        @Ctx() ctx: Context,
        @Info() info: any
    ): Promise<User> {
        passCheck(info);
        cutCheck(ctx);
        const userDataRepo = conn.manager.getRepository(UserExtendedData);
        const userRepo = conn.manager.getRepository(User);

        let user = await User.findOne(id);
        if (!user) throw new Error("User not found 404");
        const userExtendedData = new UserExtendedData();
        userExtendedData.age = age;
        userExtendedData.gender = gender;
        userExtendedData.smoker = smoker;
        userExtendedData.toothpaste = toothpaste;

        await userDataRepo.save(userExtendedData);
        user!.userExtendedDataId = userExtendedData.id;
        user!.save();

        const extdUser = await userRepo.findOne(id, { relations: ["userExtendedData"] });
        return extdUser!;
    }
    //#endregion
}