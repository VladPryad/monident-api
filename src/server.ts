require('dotenv').config();
import { createConnection } from "typeorm";
import "reflect-metadata";
import express from 'express';
import morgan from "morgan";
import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import { UserResolver } from "./resolvers/userResolver";
import { DentalResolver } from "./resolvers/dentalResolver";
import { getPayload } from './token';
import cors from 'cors';
import { IPayload } from "./types/types";
import { HL7Resolver } from "./resolvers/hl7Resolver";

const startup = async () => {

    const options: cors.CorsOptions = {
        allowedHeaders: [
            'Origin',
            'X-Requested-With',
            'Content-Type',
            'Accept',
            'X-Access-Token',
            'Authorization'
        ],
        credentials: true,
        methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
        origin: `http://localhost:${process.env.PORT}`,
        preflightContinue: false
    };

    const app = express();
    app.use(morgan("tiny"));
    app.use(function (_, __, next) {
        console.log('\x1b[33m%s\x1b[0m', "--------------------------");
        next();
    });

    app.use(cors(options));

    const conn = await createConnection();

    const server = new ApolloServer({
        schema: await buildSchema({
            resolvers: [
                UserResolver,
                DentalResolver,
                HL7Resolver],
            dateScalarMode: "isoDate",
            validate: false
        }),
        context: (req: any, res: any) => {
            const token = req.req.headers.authorization || '';
            const { payload, loggedIn } = getPayload(token);
            let id = null;
            if (!!payload)
                id = (payload as IPayload).payload.id;
            return { id, loggedIn, conn };
        }
    });

    server.applyMiddleware({ app });

    app.listen({ port: process.env.PORT }, () => {
        console.log('\x1b[44m%s\x1b[0m', `API runs at http://localhost:${process.env.PORT}${server.graphqlPath}`)
    })


}

startup();
